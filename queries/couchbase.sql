-- Generate the Couchbase document Id for a given user
Select
    'AlertPreferencesDataModel::' 
    + Lower(Convert(NVarchar(38), aspnet_UserId)) + '::' 
    + Lower(Convert(NVarchar(38), OrganizationID)) As couchbaseDocumentId
From AttestorDB.dbo.Contacts
Where EmailAddress = 'mark.angus@encrypt2.com';

/*

-----------------------------
Couchbase clean-up
-----------------------------

CREATE PRIMARY INDEX ON usersdata USING GSI;

/* fetch data. It can be reused to make sure its deleted  */
select * from  usersdata
use keys [  ]

-- delete records 
delete from  usersdata
use keys [ ]

DROP PRIMARY INDEX ON usersdata USING GSI;

*/
$email = 'amachado@trustarc.com' #Read-Host -Prompt 'Input your email'

docker run --name postgres      --restart always -p 5432:5432 -e POSTGRES_PASSWORD=postgres -e POSTGRES_USER=postgres -d postgres
docker run --name pgAdmin4      --restart always -p 5050:80 -e "PGADMIN_DEFAULT_EMAIL=$email" -e "PGADMIN_DEFAULT_PASSWORD=admin" -d dpage/pgadmin4
#docker run --name rabbitmq      --restart always -p 5672:5672 -p 15672:15672 rabbitmq
docker run --name portainer     --restart always -d -p 9000:9000 -p 8000:8000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer
#docker run --name elasticsearch --restart always -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" -e "xpack.security.enabled=false" -d docker.elastic.co/elasticsearch/elasticsearch:5.5.0
#docker run --name activemq      --restart always -p 61616:61616 -p 8161:8161 -d rmohr/activemq
docker run --name redis.devnym  --restart always -d -p 6379:6379 redis 

docker pull quay.io/testcontainers/ryuk:0.2.3
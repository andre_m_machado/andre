# https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_profiles?view=powershell-7


# change dir to Projects folder
Function prj {
	Set-Location -Path C:\Projects 
}

# change dir to TrustArc folder
Function ta {
	Set-Location -Path C:\TrustArc 
}

# stop iis, build current project, restart iis
Function bbb { 
	.\iis.ps1 -t stop 
	.\build.ps1 
	.\iis.ps1 
}

# navigate to insights build folder
function ins { set-location "C:\Projects\nymityinsights\build" }

# navigate to insights angular folder
function insa { set-location "C:\Projects\nymityinsights\Nymity.Insights\app-insight" }

Function DockerStop {
	docker stop $(docker ps -aq)
}

Function DockerRemoveContainers {
	docker rm $(docker ps -a -q)
}

# replace IDBroker configuration DB
Function idb { 
	$fileName = 'c:\projects\identitybroker\artifacts\IdentityBroker\IdentityBroker.Service\appsettings.json'
	$oldString = 'Database=IdentityBroker_Configuration;'
	$newString = 'Database=IdentityBroker_Configuration_Andre;'

	Write-Host "Replacing IdentityBroker Configuration db with $newString"
	((Get-Content -path $fileName -Raw) -replace $oldString, $newString) | Set-Content -Path $fileName; 
	
	Write-Host "Resetting IIS"
	iisreset;
}

# create a new release branch
Function cr {
	param(
		$rVersion
	)

	(git stash) -and (git co develop) -and (git pull) -and (git co -b r/$rVersion)
}

function tsaDevLogs { 
	$podName = kubectl get pods -n dev | select-string tsa | out-string | ForEach-Object { $_.Split(" ")[0].replace("`n", "").replace("`r", "") }; 
	kubectl logs -f $podName -n dev --tail 20 
}

function tsaQaLogs { 
	$podName = kubectl get pods -n qa | select-string tsa | out-string | ForEach-Object { $_.Split(" ")[0].replace("`n", "").replace("`r", "") }; 
	kubectl logs -f $podName -n qa --tail 20 
}

function poseidonDevLogs { 
	$podName = kubectl get pods -n poseidon-dev | select-string poseidon | out-string | ForEach-Object { $_.Split(" ")[0].replace("`n", "").replace("`r", "") }; 
	kubectl logs -f $podName -n poseidon-dev --tail 20 
}

function poseidonQaLogs { 
	$podName = kubectl get pods -n poseidon-qa | select-string tsa | out-string | ForEach-Object { $_.Split(" ")[0].replace("`n", "").replace("`r", "") }; 
	kubectl logs -f $podName -n poseidon-qa --tail 20 
}

Set-Alias docker-stop -Value DockerStop
Set-Alias docker-rac -Value DockerRemoveContainers

prj